## Handwriting acquisition software – HandAQUS.

Software for handwriting acquisition for Windows using Wacom digitalizing tablets and WinTAB API.

Primary usage for research and medical issues. 


### Install
Just copy ALL files, and then run it via **HandAQUS.exe** 

### Prerequisites
1. Installed latest WaCom drivers -> https://www.wacom.com/en/support/product-support/drivers
2. Digitalizing tablets connected to PC!

**``NOTE! If this two conditions will not be accomplished, software will NOT start.``**

### Features
1. Handwriting acquisition
1. Save/Load
1. Auto-generate file names
1. AutoSave
1. Clear 

### Screenshot
![picture](image.png)


In case of problems/questions contact @muchajano@phd.feec.vutbr.cz. 

#### Acknowledgements
This work was supported by the grant of the Czech Science Foundation 18-16835S (Research of advanced developmental dysgraphia diagnosis and rating methods based on quantitative analysis of online handwriting and drawing).
